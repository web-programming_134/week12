import {
  IsNotEmpty,
  IsNumber,
  IsPositive,
  IsString,
  Length,
} from 'class-validator';

export class CreateCustomerDto {
  @IsString()
  @IsNotEmpty()
  @Length(4, 36)
  name: string;

  @IsNumber()
  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNumber()
  @IsNotEmpty()
  @Length(10, 10)
  tel: string;

  @IsString()
  @IsNotEmpty()
  @Length(1, 1)
  gender: string;
}
